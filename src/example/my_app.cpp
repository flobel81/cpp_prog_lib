/**
 * MIT License
 *
 * Copyright (c) 2022 Florian Belser
 **/

#include "my_app.hpp"

namespace MyOwnApplication
{
void MyApplication::Help(void)
{
   auto c_TmpParas = GetParameterList();
   auto c_Version = GetVersionRaw();

   std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++++"
             << std::endl;
   std::cout << "This is my special Help of my special application" << std::endl
             << std::endl;
   std::cout << "Application version: " << std::endl
             << "Major:"
             << std::to_string(c_Version[Application::VERSION_MAJOR])
             << std::endl
             << "Minor:"
             << std::to_string(c_Version[Application::VERSION_MINOR])
             << std::endl
             << "Revision:"
             << std::to_string(c_Version[Application::VERSION_REVISION])
             << std::endl;
   if (false == GetLicense().empty())
   {
      std::cout << "Application license: " << GetLicense() << std::endl;
   }
   std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++++"
             << std::endl;
   for (auto it = c_TmpParas.begin(); it != c_TmpParas.end(); ++it)
   {
      std::cout << "--" << it->GetLong() << " (-" << it->GetShort() << ")"
                << std::endl;
      std::cout << "   " << it->GetDescription() << std::endl;
   }
   exit(0);
}
}  // namespace MyOwnApplication