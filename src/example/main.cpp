/**
 * MIT License
 *
 * Copyright (c) 2022 Florian Belser
 **/

#include <atomic>
#include <chrono>
#include <csignal>
#include <cstdint>
#include <iostream>
#include <thread>

#include "analytics.hpp"
#include "app_config.hpp"
#include "application.hpp"
#include "debug.hpp"
#include "my_app.hpp"
#include "timestamp.hpp"
#include "toml.hpp"

// Function prototypes
static void m_SignalHandler(int osn_SigNum);
static void m_ConfigurationFileTomlParser(std::string os_FilePath);
static void m_AppTimeout(std::string os_Timeout);
static void m_SetDebugMode(std::string os_DebugMode);
static void m_Verbose();

// module global variables
static std::string mc_LogFile{};
static uint32_t mu32_Timeout = 0;
static Application::Debug_Mode mc_DebugMode{Application::Debug_Mode::OFF};
static bool mb_Verbose = false;

static std::atomic_bool mb_quit = false;  // signal flag

// Implementation
/**
 * @brief Signal handler of the application
 *
 * @param osn_SigNum
 */
static void m_SignalHandler(int osn_SigNum)
{
   if (osn_SigNum != 0)
      mb_quit.store(true);
}

/**
 * @brief Set Verbose mode to true
 *
 */
static void m_Verbose()
{
   mb_Verbose = true;
}

/**
 * @brief Set debug mode of the Demo application
 *
 * @param os_DebugMode
 */
static void m_SetDebugMode(std::string os_DebugMode)
{
   if (os_DebugMode == "ON")
   {
      mc_DebugMode = Application::Debug_Mode::ON;
   }
   else if (os_DebugMode == "EXTENDED")
   {
      mc_DebugMode = Application::Debug_Mode::EXTENDED;
   }
   else if (os_DebugMode == "MAX")
   {
      mc_DebugMode = Application::Debug_Mode::MAX;
   }
   else
   {
      // In case of OFF or wrong input, set to OFF
      mc_DebugMode = Application::Debug_Mode::OFF;
   }
}

/**
 * @brief Callback function for parsing the application specific toml based
 * configuration file
 *
 * @param os_FilePath path to toml configuration file
 */
static void m_ConfigurationFileTomlParser(std::string os_FilePath)
{
   Analytics::Timer<std::milli> timer{};
   const auto c_Data = toml::parse(os_FilePath);
   if (c_Data.contains("debug"))
   {
      const auto& r_TomlDebug = toml::find(c_Data, "debug");
      if (r_TomlDebug.is_table())
      {
         if (r_TomlDebug.contains("LogFile"))
         {
            mc_LogFile = toml::find<std::string>(r_TomlDebug, "LogFile");
         }
         if (r_TomlDebug.contains("Mode"))
         {
            m_SetDebugMode(toml::find<std::string>(r_TomlDebug, "Mode"));
         }
      }
   }
}

/**
 * @brief Set timeout for the demo application. After given time, the
 * application automatically stops
 *
 * @param os_Timeout Timeout in seconds (value of 0 means run forever)
 */
static void m_AppTimeout(std::string os_Timeout)
{
   try
   {
      mu32_Timeout = std::stoi(os_Timeout);
   }
   catch (const std::exception& e)
   {
      std::cout << e.what() << " failed, use no timeout" << std::endl;
      mu32_Timeout = 0;
   }
}

/**
 * @brief Main function of the application
 *
 * @param argc
 * @param argv
 * @return int
 */
int main(int argc, char* argv[])
{
   Analytics::Timer timer{};

   struct sigaction t_SigAction;
   sigemptyset(&t_SigAction.sa_mask);
   t_SigAction.sa_handler = m_SignalHandler;
   t_SigAction.sa_flags = 0;
   sigaction(SIGINT, &t_SigAction, NULL);

   std::chrono::time_point<std::chrono::steady_clock> c_Start;
   std::chrono::time_point<std::chrono::steady_clock> c_Now;

   Application::AppDebug c_Debug = std::make_shared<Application::Debug>();
   std::tuple<int, int, int> c_Version{std::stoi(AppInfo::APP_VER_MAJOR),
                                       std::stoi(AppInfo::APP_VER_MINOR),
                                       std::stoi(AppInfo::APP_VER_PATCH)};
   std::string c_License = "MIT license";

#ifndef TEST_SYS
   Application::Info c_App{AppInfo::APP_NAME, c_Version, c_Debug};
#else
   MyOwnApplication::MyApplication c_App{"Test Application, own help",
                                         c_Version, c_Debug};
#endif
   c_App.SetLicense(c_License);

   try
   {
      c_App.SetParameter('f', "file", "Configuration file",
                         &m_ConfigurationFileTomlParser, nullptr);
      c_App.SetParameter("timeout", "Timeout after x seconds", &m_AppTimeout,
                         nullptr);
      c_App.SetParameter('V', "Verbose makes the same as debug", nullptr,
                         &m_Verbose);
      c_App.ParseCmdLine(argc, argv);
   }
   catch (const std::bad_function_call& e)
   {
      c_Debug->Print(
          std::string{"Parse command line error: " + std::string{e.what()}});
      return 1;
   }
   catch (const std::invalid_argument& e)
   {
      c_Debug->Print(std::string{"Argument error: " + std::string{e.what()}});
      return 1;
   }
   catch (const std::runtime_error& e)
   {
      c_Debug->Print(std::string{"Runtime error: " + std::string{e.what()}});
      return 1;
   }
   catch (const toml::syntax_error& e)
   {
      c_Debug->Print(
          std::string{"Toml file syntax error: " + std::string{e.what()}});
      return 1;
   }
   catch (const std::exception& e)
   {
      c_Debug->Print(std::string{e.what()});
      return 1;
   }

   // set debug output
   c_Debug->SetLogFile(mc_LogFile);
   if (c_Debug->GetDebugMode() == Application::Debug_Mode::OFF)
   {
      c_Debug->SetDebugMode(mc_DebugMode);
   }

   // check verbose flag
   if (mb_Verbose == true)
   {
      c_Debug->IncreaseDebugMode();
   }

   if (mu32_Timeout != 0)
   {
      c_Start = std::chrono::steady_clock::now();
   }
   std::cout << "------------------------------" << std::endl;
   std::cout << "Use functor version : " << c_App << std::endl;
   std::cout << "------------------------------" << std::endl;
   c_Debug->Print("------------------------------");
   c_Debug->Print(std::string{"Use default version : " + c_App.GetVersion()});
   c_Debug->Print(Application::Version::GetLibVersion());
   c_Debug->Print("------------------------------");

   c_Debug->PrintDebug(
       std::string{"ISO conform timestamp default milliseconds: " +
                   Timestamp::GetISO8601()});
   c_Debug->PrintDebug(
       std::string{"ISO conform timestamp microseconds: " +
                   Timestamp::GetISO8601<std::chrono::microseconds>()});
   c_Debug->PrintDebug(
       std::string{"ISO conform timestamp seconds: " +
                   Timestamp::GetISO8601<std::chrono::seconds>()});
   c_Debug->PrintDebug(
       std::string{"ISO conform timestamp minutes: " +
                   Timestamp::GetISO8601<std::chrono::minutes>()});
   c_Debug->PrintDebug(
       std::string{"ISO conform timestamp hours: " +
                   Timestamp::GetISO8601<std::chrono::hours>()});

   while (mb_quit.load() != true)
   {
      std::this_thread::sleep_for(std::chrono::milliseconds(100));
      if (mu32_Timeout != 0)
      {
         c_Now = std::chrono::steady_clock::now();
         if (std::chrono::duration_cast<std::chrono::seconds>(c_Now - c_Start)
                 .count() >= mu32_Timeout)
         {
            c_Debug->PrintDebug("Application timeout reached");
            mb_quit.store(true);
         }
      }
   }

   return 0;
}