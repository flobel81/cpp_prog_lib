/*
 * Copyright (C) 2021-2022 The developers
 *
 * Author:
 *  Florian Belser
 *
 * Source:
 *  https://github.com/flobel81/cpp_prog_lib.git
 *
 * This file is a part of the cpp_prog_lib library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 */

#ifndef __APP_CONFIG
#define __APP_CONFIG

#include <iostream>

namespace AppInfo
{
const std::string APP_NAME{"Demo_Application"};
const std::string APP_VER{"0.2.0"};
const std::string APP_VER_MAJOR{"0"};
const std::string APP_VER_MINOR{"2"};
const std::string APP_VER_PATCH{"0"};

}  // namespace AppInfo
#endif  // __APP_CONFIG
