/**
 * MIT License
 *
 * Copyright (c) 2022 Florian Belser
 **/

#ifndef __MY_APP_H__
#define __MY_APP_H__

#include "application.hpp"

namespace MyOwnApplication
{
/**
 * @brief My own demo application
 *
 */
class MyApplication : public Application::Info
{
  public:
   /**
    * @brief Help function of my own application
    *
    */
   virtual void Help(void) override;

   /**
    * @brief Construct a new My Application object
    *
    * @param oc_Application_Name
    * @param roc_Version
    * @param roc_Debug
    */
   MyApplication(std::string oc_Application_Name,
                 std::tuple<int, int, int>& roc_Version,
                 const Application::AppDebug& roc_Debug)
       : Application::Info::Info{oc_Application_Name, roc_Version,
                                 roc_Debug} {};
};
}  // namespace MyOwnApplication
#endif  // __MY_APP_H__