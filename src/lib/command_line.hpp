/*
 * Copyright (C) 2021-2022 The developers
 *
 * Author:
 *  Florian Belser
 *
 * Source:
 *  https://github.com/flobel81/cpp_prog_lib.git
 *
 * This file is a part of the cpp_prog_lib library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 */

#ifndef __COMMAND_LINE_H  // Headerguard
#define __COMMAND_LINE_H

#include <getopt.h>

#include <algorithm>
#include <array>
#include <functional>
#include <iostream>
#include <memory>
#include <tuple>
#include <vector>

namespace CommandLine
{
/**
 * @brief Command Line Parameter data type for handling command line parameters
 *
 */
struct Parameter
{
  public:
   /**
    * @brief Construct a new Parameter object
    *
    *
    * @param oc_Short              Command line parameter short option
    * @param oc_Long               Command line parameter long option
    * @param oc_Description        Brief description of the command
    * @param ob_ArgRequired        Parameter argument required [true / false]
    * @param opf_ParameterFunction Function call for application parameters
    * which requires a parameter. In that case the parameter is passed by
    * function parameter as type of string
    * @param opf_VoidFunction      Function call for application parameters
    * which does not require a parameter.
    */
   Parameter(char oc_Short, std::string oc_Long, std::string oc_Description,
             bool ob_ArgRequired,
             std::function<void(std::string)> opf_ParameterFunction,
             std::function<void()> opf_VoidFunction);

   /**
    * @brief Get the short option of the command line parameter object
    *
    * @return const char&
    */
   const char &GetShort(void) const;

   /**
    * @brief Get the long option of the command line parameter object
    *
    * @return const std::string&
    */
   const std::string &GetLong(void) const;

   /**
    * @brief Get the long option of the command line parameter object as char *
    *
    * @return cstd::shared_ptr<char>
    */
   const std::shared_ptr<char> GetLongChar(void) const;

   /**
    * @brief Get the description of the command line parameter object
    *
    * @return const std::string&
    */
   const std::string &GetDescription(void) const;

   /**
    * @brief Get the Arg Required parameter of the command line parameter object
    *
    * @return true
    * @return false
    */
   bool GetArgRequired(void) const;

   /**
    * @brief Return the function call for application parameter
    * which require a parameter of the command line parameter
    *
    * @return std::function<void(std::string)>
    */
   std::function<void(std::string)> GetParaFunction(void) const;

   /**
    * @brief Return the function call for application parameter
    * which does not require a parameter of the command line parameter object
    *
    * @return std::function<void()>
    */
   std::function<void()> GetVoidFunction(void) const;

   /**
    * @brief Overloaded < operator for sort functionality
    *
    * @param roc_Left    Left hand parameter for comparison
    * @param roc_Right   Right hand parameter for comparison
    * @return true
    * @return false
    */
   friend bool operator<(const Parameter &roc_Left, const Parameter &roc_Right)
   {
      return (std::tolower(roc_Left.c_Short) < std::tolower(roc_Right.c_Short));
   }

  private:
   char c_Short;
   std::string c_Long;
   std::string c_Description;
   bool b_ArgRequired;
   std::shared_ptr<char> pt_LongName;
   std::function<void(std::string)> pf_ParameterFunction;
   std::function<void()> pf_VoidFunction;
};

/**
 * @brief Command Line Parameter List data type for handling all parameters
 *
 */
struct ParameterList
{
  public:
   /**
    * @brief Add one Parameter object to Parameter List
    *
    * @param oc_Short              Command line parameter short option
    * @param oc_Long               Command line parameter long option
    * @param oc_Description        Brief description of the command
    * @param opf_ParameterFunction Function call for application parameters
    * which requires a parameter. In that case the parameter is passed by
    * function parameter as type of string
    * @param opf_VoidFunction      Function call for application parameters
    * which does not require a parameter.
    *
    * @throws invalid_argument
    */
   void SetParameter(char oc_Short, std::string oc_Long,
                     std::string oc_Description,
                     std::function<void(std::string)> opf_ParameterFunction,
                     std::function<void()> opf_VoidFunction);

   /**
    * @brief Add one Parameter object to Parameter List
    *
    * @param oc_Short              Command line parameter short option
    * @param oc_Description        Brief description of the command
    * @param opf_ParameterFunction Function call for application parameters
    * which requires a parameter. In that case the parameter is passed by
    * function parameter as type of string
    * @param opf_VoidFunction      Function call for application parameters
    * which does not require a parameter.
    *
    * @throws invalid_argument
    */
   void SetParameter(char oc_Short, std::string oc_Description,
                     std::function<void(std::string)> opf_ParameterFunction,
                     std::function<void()> opf_VoidFunction);

   /**
    * @brief Add one Parameter object to Parameter List
    *
    * @param oc_Long               Command line parameter long option
    * @param oc_Description        Brief description of the command
    * @param opf_ParameterFunction Function call for application parameters
    * which requires a parameter. In that case the parameter is passed by
    * function parameter as type of string
    * @param opf_VoidFunction      Function call for application parameters
    * which does not require a parameter.
    *
    * @throws invalid_argument
    */
   void SetParameter(std::string oc_Long, std::string oc_Description,
                     std::function<void(std::string)> opf_ParameterFunction,
                     std::function<void()> opf_VoidFunction);
   /**
    * @brief Get the vector object of all Parameters
    *
    * @return std::vector <Parameter>      List of available parameters -->
    * vector container
    */
   std::vector<Parameter> GetParameterList(void) const;

  protected:
   /**
    * @brief Construct a new Parameter List object
    *
    */
   ParameterList() = default;

   /**
    * @brief Destroy the Parameter List object
    *
    */
   ~ParameterList() = default;

  private:
   std::vector<Parameter> c_ParameterList{};
};

/**
 * @brief Parser class which provides functionality to parse the command line
 *
 */
class Parser : public ParameterList
{
  public:
   /**
    * @brief Parse the command line parameters
    *
    * @param argc
    * @param argv
    *
    * @throw out_of_range            Command Line Parameter unknown
    * @throw bad_function_call       No function call initialized
    */
   void ParseCmdLine(int argc, char *argv[]);

  protected:
   /**
    * @brief Construct a new Parser object
    *
    */
   Parser() : ParameterList(){};

   /**
    * @brief Destroy the Parser object
    *
    */
   ~Parser() = default;

   /**
    * @brief Create a Cmd Line List Short object
    *
    * @return std::string     String of allowed short command line list values
    */
   std::string CreateCmdLineListShort(void) const;

   /**
    * @brief Create a Cmd Line List Long object
    *
    * @return std::unique_ptr<option[]>      unique_ptr of struct option array.
    * Responsibility will be moved to caller function
    */
   std::unique_ptr<option[]> CreateCmdLineListLong(void);
};

}  // namespace CommandLine

#endif  // __COMMAND_LINE_H Headerguard
