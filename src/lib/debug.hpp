/*
 * Copyright (C) 2021-2022 The developers
 *
 * Author:
 *  Florian Belser
 *
 * Source:
 *  https://github.com/flobel81/cpp_prog_lib.git
 *
 * This file is a part of the cpp_prog_lib library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 */

#ifndef __DEBUG  // Headerguard
#define __DEBUG

#include <fstream>
#include <functional>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>

#include "debug_facility.hpp"

namespace Application
{
/**
 * @brief Debug Class
 *
 */
class Debug : public iDebugFacility
{
  public:
   /**
    * @brief Construct a new Debug object
    *
    * @param oc_DebugMode     Debug Mode --> See enum class DebugMode
    * @param oc_FilePath      Path to log file
    */
   Debug(Debug_Mode oc_DebugMode, std::string oc_FilePath);

   /**
    * @brief Construct a new debug object
    *
    */
   Debug();

   /**
    * @brief Destroy the Debug object
    *
    */
   ~Debug();

   /**
    * @brief Increse current debug mode if possible
    *
    */
   void IncreaseDebugMode(void) override;

   /**
    * @brief Get the Debug Level object
    *
    * @return Debug    Return current debug level
    */
   Debug_Mode GetDebugMode(void) const override;

   /**
    * @brief Set the Debug Mode object
    *
    * @param roc_DebugMode DEBUG MODE which shall be set
    */
   void SetDebugMode(const Debug_Mode& roc_DebugMode) override;

   /**
    * @brief Set the Log File object
    *
    * @param roc_LogFile Path to log file
    */
   void SetLogFile(const std::string& roc_LogFile) override;

   /**
    * @brief Output debug string if debug is activated including
    *        file name and line of calling function
    *
    * @param oc_String        String which shall be printed
    * @param opcn_File        Automatically filled
    * @param opcn_Function    Automatically filled
    * @param osnLine          Automatically filled
    */
   void PrintDebug(std::string oc_String,
                   char const* opcn_File = __builtin_FILE(),
                   char const* opcn_Function = __builtin_FUNCTION(),
                   int osnLine = __builtin_LINE()) override;

   /**
    * @brief Print the oc_String. Print string regardless of
    * debug mode is activated or not. If debug is activated,
    * file name and line of calling function is added
    *
    * @param oc_String        String which shall be printed
    * @param opcn_File        Automatically filled
    * @param opcn_Function    Automatically filled
    * @param osnLine          Automatically filled
    */
   void Print(std::string oc_String, char const* opcn_File = __builtin_FILE(),
              char const* opcn_Function = __builtin_FUNCTION(),
              int osnLine = __builtin_LINE()) override;

  private:
   Debug_Mode c_DebugMode;
   std::ofstream c_OutputFile;
   std::ostream c_Output;
};

}  // namespace Application

#endif  // __DEBUG Headerguard
