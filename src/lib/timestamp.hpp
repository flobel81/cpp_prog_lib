/*
 * Copyright (C) 2021-2022 The developers
 *
 * Author:
 *  Florian Belser
 *
 * Source:
 *  https://github.com/flobel81/cpp_prog_lib.git
 *
 * This file is a part of the cpp_prog_lib library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 */

#ifndef __TIMESTAMP_H__
#define __TIMESTAMP_H__

#include <chrono>
#include <string>

namespace Timestamp
{
/**
 * @brief Template function which return ISO8601 conform timestamp
 *
 * @tparam Precision       Precision which shall be used for timestamp
 * @return std::string     Return ISO8601 conform timestamp
 */
template <typename Precision = std::chrono::milliseconds>
std::string GetISO8601(void);

}  // namespace Timestamp
#endif  // __TIMESTAMP_H__