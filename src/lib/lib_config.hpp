/*
 * Copyright (C) 2021-2022 The developers
 *
 * Author:
 *  Florian Belser
 *
 * Source:
 *  https://github.com/flobel81/cpp_prog_lib.git
 *
 * This file is a part of the cpp_prog_lib library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 */

#ifndef __LIB_CONFIG
#define __LIB_CONFIG

#include <iostream>

namespace LibInfo
{
const std::string LIBRARY_NAME{"cpp_prog_base_lib"};
const std::string LIBRARY_VER{"1.4.0"};
const std::string LIBRARY_VER_MAJOR{"1"};
const std::string LIBRARY_VER_MINOR{"4"};
const std::string LIBRARY_VER_PATCH{"0"};

}  // namespace LibInfo
#endif  // __LIB_CONFIG
