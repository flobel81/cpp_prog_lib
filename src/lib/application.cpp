/*
 * Copyright (C) 2021-2022 The developers
 *
 * Author:
 *  Florian Belser
 *
 * Source:
 *  https://github.com/flobel81/cpp_prog_lib.git
 *
 * This file is a part of the cpp_prog_lib library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 */

#include "application.hpp"

#include <unistd.h>

namespace Application
{
// --------------- CLASS INFO -----------------------------------
// --------------------------------------------------------------
Info::Info(std::string oc_Application_Name,
           std::tuple<int, int, int> &roc_Verison, const AppDebug &roc_Debug)
    : CommandLine::Parser(),
      Application::Version(
          std::get<Application::VERSION_MAJOR>(roc_Verison),
          std::get<Application::VERSION_MINOR>(roc_Verison),
          std::get<Application::VERSION_REVISION>(roc_Verison)),
      c_Application_Name{oc_Application_Name},
      c_Debug{roc_Debug}
{
   // set default param
   SetParameter('v', "version", "This is the version", nullptr,
                std::bind(&Application::Info::PrintVersionInt, this));
   SetParameter('h', "help", "Print the help", nullptr,
                std::bind(&Application::Info::Help, this));
   SetParameter('d', "debug", "Activate debug", nullptr,
                std::bind(&Application::Info::IncreaseDebugLevelInt, this));
   SetParameter('m', "manpage", "Print the manpage", nullptr,
                std::bind(&Application::Info::Manpage, this));
}

void Info::PrintVersionInt() const
{
   std::cout << GetVersion() << std::endl;
   exit(0);
}

void Info::IncreaseDebugLevelInt() const
{
   c_Debug->IncreaseDebugMode();
}

void Info::Help()
{
   std::string c_TmpShort{};
   std::string c_TmpLong{};
   std::vector<CommandLine::Parameter> c_ParameterList = GetParameterList();
   std::cout << "--------------------------------------------------------------"
             << std::endl;
   std::cout << "This is the Help of " << c_Application_Name << ":"
             << std::endl;
   std::cout << std::endl;

   std::cout << "Command Line Parameter: " << CreateCmdLineListShort()
             << std::endl;
   for (auto it = c_ParameterList.begin(); it != c_ParameterList.end(); ++it)
   {
      c_TmpLong = it->GetLong().empty() != true ? "\t--" + it->GetLong() : "";
      c_TmpShort =
          it->GetShort() != '\0' ? "-" + std::string{it->GetShort()} : "";
      std::cout << c_TmpShort << c_TmpLong << std::endl;
   }
   std::cout << "--------------------------------------------------------------"
             << std::endl;
   exit(0);
}

void Info::Manpage()
{
   std::string c_TmpShort{};
   std::string c_TmpLong{};

   std::vector<CommandLine::Parameter> c_ParameterList = GetParameterList();
   std::cout << "--------------------------------------------------------------"
             << std::endl;
   std::cout << "This is the Manpage of " << c_Application_Name << ":"
             << std::endl;
   std::cout << std::endl;

   std::cout << "Application version: " << GetVersion() << std::endl;
   std::cout << GetLibVersion() << std::endl;
   if (false == c_License.empty())
   {
      std::cout << "Application license: " << GetLicense() << std::endl;
   }
   std::cout << std::endl;
   std::cout << "Command Line Parameter: " << CreateCmdLineListShort()
             << std::endl;

   for (auto it = c_ParameterList.begin(); it != c_ParameterList.end(); ++it)
   {
      c_TmpLong = it->GetLong().empty() != true ? "\t--" + it->GetLong() : "";
      c_TmpShort =
          it->GetShort() != '\0' ? "-" + std::string{it->GetShort()} : "";
      std::cout << std::endl << it->GetDescription() << ":" << std::endl;
      std::cout << c_TmpShort << c_TmpLong << std::endl;
   }
   std::cout << "--------------------------------------------------------------"
             << std::endl;
   exit(0);
}

std::string Info::GetLicense(void) const
{
   return c_License;
}

void Info::SetLicense(std::string &roc_License)
{
   c_License = roc_License;
}

}  // namespace Application