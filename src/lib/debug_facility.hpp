/*
 * Copyright (C) 2021-2022 The developers
 *
 * Author:
 *  Florian Belser
 *
 * Source:
 *  https://github.com/flobel81/cpp_prog_lib.git
 *
 * This file is a part of the cpp_prog_lib library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 */

#ifndef __DEBUG_FACILITY_H__
#define __DEBUG_FACILITY_H__

#include <iostream>
#include <memory>

namespace Application
{
enum class Debug_Mode
{
   OFF = 0,
   ON = 1,
   EXTENDED = 2,
   MAX = 3
};

/**
 * @brief Interface class for debugging
 *
 * Interface encapsulate any kind of debugging lib or own implementation.
 * This feature is realized through the dependency injection pattern
 */
class iDebugFacility
{
  public:
   virtual ~iDebugFacility() = default;

   virtual void IncreaseDebugMode() = 0;

   virtual Debug_Mode GetDebugMode() const = 0;

   virtual void SetDebugMode(const Debug_Mode& roc_DebugMode) = 0;

   virtual void SetLogFile(const std::string& roc_LogFile) = 0;

   virtual void PrintDebug(std::string oc_String,
                           char const* opcn_File = __builtin_FILE(),
                           char const* opcn_Function = __builtin_FUNCTION(),
                           int osnLine = __builtin_LINE()) = 0;

   virtual void Print(std::string oc_String,
                      char const* opcn_File = __builtin_FILE(),
                      char const* opcn_Function = __builtin_FUNCTION(),
                      int osnLine = __builtin_LINE()) = 0;
};

using AppDebug = std::shared_ptr<iDebugFacility>;
}  // End Namespace Application

#endif  // __DEBUG_FACILITY_H__