/*
 * Copyright (C) 2021-2022 The developers
 *
 * Author:
 *  Florian Belser
 *
 * Source:
 *  https://github.com/flobel81/cpp_prog_lib.git
 *
 * This file is a part of the cpp_prog_lib library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 */

#include "command_line.hpp"

#include <functional>
#include <iomanip>

namespace CommandLine
{
// ------------ STRUCT parameter ---------------------------
// ---------------------------------------------------------
Parameter::Parameter(char oc_Short, std::string oc_Long,
                     std::string oc_Description, bool ob_ArgRequired,
                     std::function<void(std::string)> opf_ParameterFunction,
                     std::function<void()> opf_VoidFunction)
    : c_Short{oc_Short},
      c_Long{oc_Long},
      c_Description{oc_Description},
      b_ArgRequired{ob_ArgRequired},
      pf_ParameterFunction{opf_ParameterFunction},
      pf_VoidFunction{opf_VoidFunction}
{
   pt_LongName = std::shared_ptr<char>(new char[c_Long.size() + 1],
                                       std::default_delete<char[]>());
   c_Long.copy(pt_LongName.get(), c_Long.size() + 1);
   pt_LongName.get()[c_Long.size()] = '\0';
}

const char& Parameter::GetShort(void) const
{
   return c_Short;
}

const std::string& Parameter::GetLong(void) const
{
   return c_Long;
}

const std::shared_ptr<char> Parameter::GetLongChar(void) const
{
   return pt_LongName;
}

const std::string& Parameter::GetDescription(void) const
{
   return c_Description;
}

bool Parameter::GetArgRequired(void) const
{
   return b_ArgRequired;
}

std::function<void(std::string)> Parameter::GetParaFunction(void) const
{
   return pf_ParameterFunction;
}

std::function<void()> Parameter::GetVoidFunction(void) const
{
   return pf_VoidFunction;
}

// ------------ STRUCT parameter list ----------------------
// ---------------------------------------------------------
std::vector<Parameter> ParameterList::GetParameterList(void) const
{
   return c_ParameterList;
}

void ParameterList::SetParameter(
    char oc_Short, std::string oc_Long, std::string oc_Description,
    std::function<void(std::string)> opf_ParameterFunction,
    std::function<void()> opf_VoidFunction)
{
   bool b_ArgReq = false;
   std::string c_Tmp = "Could not add parameter " + std::string{oc_Short} +
                       " (" + oc_Long + ")";
   for (auto c_Iterator = c_ParameterList.begin();
        c_Iterator != c_ParameterList.end(); ++c_Iterator)
   {
      if (((c_Iterator->GetShort() == oc_Short) && (oc_Short != '\0')) ||
          ((c_Iterator->GetLong() == oc_Long) && (oc_Long.empty() != true)))
      {
         throw std::invalid_argument(c_Tmp.c_str());
      }
   }
   if ((opf_ParameterFunction != nullptr) && (opf_VoidFunction == nullptr))
   {
      b_ArgReq = true;
   }
   else if ((opf_VoidFunction != nullptr) && (opf_ParameterFunction == nullptr))
   {
      b_ArgReq = false;
   }
   else
   {
      throw std::invalid_argument(c_Tmp.c_str());
   }
   c_ParameterList.emplace_back(oc_Short, oc_Long, oc_Description, b_ArgReq,
                                opf_ParameterFunction, opf_VoidFunction);

   // sort the vector
   std::sort(c_ParameterList.begin(), c_ParameterList.end());
}

void ParameterList::SetParameter(
    char oc_Short, std::string oc_Description,
    std::function<void(std::string)> opf_ParameterFunction,
    std::function<void()> opf_VoidFunction)
{
   SetParameter(oc_Short, "", oc_Description, opf_ParameterFunction,
                opf_VoidFunction);
}

void ParameterList::SetParameter(
    std::string oc_Long, std::string oc_Description,
    std::function<void(std::string)> opf_ParameterFunction,
    std::function<void()> opf_VoidFunction)
{
   SetParameter('\0', oc_Long, oc_Description, opf_ParameterFunction,
                opf_VoidFunction);
}

// ------------ CLASS Parser--------------------------------
// ---------------------------------------------------------
std::string Parser::CreateCmdLineListShort(void) const
{
   std::string c_CmdLineList{};
   std::vector<CommandLine::Parameter> c_ParameterList = GetParameterList();
   for (auto c_Iterator = c_ParameterList.begin();
        c_Iterator != c_ParameterList.end(); ++c_Iterator)
   {
      if (c_Iterator->GetShort() != '\0')
      {
         c_CmdLineList += c_Iterator->GetShort();
         if (c_Iterator->GetArgRequired() == true)
         {
            c_CmdLineList += ":";
         }
      }
   }
   return c_CmdLineList;
}

std::unique_ptr<option[]> Parser::CreateCmdLineListLong(void)
{
   std::vector<CommandLine::Parameter> c_ParameterList = GetParameterList();
   auto t_Size = c_ParameterList.size() + 1;
   std::unique_ptr<option[]> pt_array = std::make_unique<option[]>(t_Size);
   int i = 0;
   for (auto c_Iterator = c_ParameterList.begin();
        c_Iterator != c_ParameterList.end(); ++c_Iterator)
   {
      // Get long
      if (c_Iterator->GetLong().empty() != true)
      {
         pt_array[i].name = c_Iterator->GetLongChar().get();
      }
      else
      {
         pt_array[i].name = &c_Iterator->GetShort();
      }

      // Arg required?
      pt_array[i].has_arg = c_Iterator->GetArgRequired() == true
                                ? required_argument
                                : no_argument;

      // Flag always 0
      pt_array[i].flag = nullptr;

      // Get short
      if (c_Iterator->GetShort() != '\0')
      {
         pt_array[i].val = c_Iterator->GetShort();
      }
      else
      {
         pt_array[i].val = 0;
      }
      ++i;
   }
   // finish vector
   pt_array[i].name = nullptr;
   pt_array[i].has_arg = no_argument;
   pt_array[i].flag = nullptr;
   pt_array[i].val = 0;

   return pt_array;
}

void Parser::ParseCmdLine(int argc, char* argv[])
{
   int opt = 0;
   int long_index = 0;
   std::vector<CommandLine::Parameter> c_ParameterList = GetParameterList();
   std::vector<CommandLine::Parameter>::iterator c_Iterator;
   // Get CmdLineLIst
   std::string c_CmdLineList = CreateCmdLineListShort();
   auto pt_LongOptions = CreateCmdLineListLong();

   while (true)
   {
      opt = getopt_long(argc, argv, c_CmdLineList.c_str(), &pt_LongOptions[0],
                        &long_index);
      if (-1 == opt)
      {
         // end of argument list
         break;
      }
      else if (0 == opt)
      {
         // long option handling
         c_Iterator = std::find_if(
             c_ParameterList.begin(), c_ParameterList.end(),
             [&](const CommandLine::Parameter& C_Parameter) {
                return C_Parameter.GetLong() == pt_LongOptions[long_index].name;
             });
      }
      else
      {
         // short option handling
         c_Iterator =
             std::find_if(c_ParameterList.begin(), c_ParameterList.end(),
                          [&](const CommandLine::Parameter& C_Parameter) {
                             return C_Parameter.GetShort() == opt;
                          });
      }

      if (c_Iterator != c_ParameterList.end())
      {
         // found parameter, move on
         if (c_Iterator->GetArgRequired() == true)
         {
            auto pf_ParameterFunction = c_Iterator->GetParaFunction();
            (pf_ParameterFunction != nullptr) ? pf_ParameterFunction(optarg)
                                              : throw std::bad_function_call();
         }
         else
         {
            auto pf_VoidFunction = c_Iterator->GetVoidFunction();
            (pf_VoidFunction != nullptr) ? pf_VoidFunction()
                                         : throw std::bad_function_call();
         }
      }
      else
      {
         // parameter not available or invalid
         throw std::out_of_range("Application parameter out of range");
      }
   }
}

}  // namespace CommandLine