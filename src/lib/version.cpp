/*
 * Copyright (C) 2021-2022 The developers
 *
 * Author:
 *  Florian Belser
 *
 * Source:
 *  https://github.com/flobel81/cpp_prog_lib.git
 *
 * This file is a part of the cpp_prog_lib library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 */

#include "version.hpp"

#include <iomanip>

#include "lib_config.hpp"

namespace Application
{
std::string Version::GetLibVersion(void)
{
   std::ostringstream stream_Tmp;
   stream_Tmp << LibInfo::LIBRARY_NAME << ": "
              << "v" << LibInfo::LIBRARY_VER_MAJOR << "_" << std::setfill('0')
              << std::setw(2) << LibInfo::LIBRARY_VER_MINOR << std::setw(1)
              << "r" << LibInfo::LIBRARY_VER_PATCH;
   return stream_Tmp.str();
}

std::string Version::GetVersion(void) const
{
   std::ostringstream stream_Tmp;
   stream_Tmp << "v" << au8_Version[VERSION_MAJOR] << "_" << std::setfill('0')
              << std::setw(2) << au8_Version[VERSION_MINOR] << std::setw(1)
              << "r" << au8_Version[VERSION_REVISION];
   return stream_Tmp.str();
}

std::array<int, 3> Version::GetVersionRaw(void) const
{
   return au8_Version;
}

std::ostream& Version::PrintVersion(std::ostream& ros_OutputStream) const
{
   return ros_OutputStream << GetVersion();
}

}  // namespace Application