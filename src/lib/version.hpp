/*
 * Copyright (C) 2021-2022 The developers
 *
 * Author:
 *  Florian Belser
 *
 * Source:
 *  https://github.com/flobel81/cpp_prog_lib.git
 *
 * This file is a part of the cpp_prog_lib library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 */

#ifndef __VERSION_H  // Headerguard
#define __VERSION_H

#include <array>
#include <iostream>

namespace Application
{
const uint8_t VERSION_MAJOR = 0;
const uint8_t VERSION_MINOR = 1;
const uint8_t VERSION_REVISION = 2;

/**
 * @brief Version class which handles the version information of the application
 *
 */
class Version
{
  public:
   /**
    * @brief Get the Lib Version object
    *
    * static member function of class Version which can be accessed without
    * having an initialized object of the class
    *
    * @return std::string  Return nice formatted version
    *                      of the library (<library_name>:vx_xxrx)
    */
   static std::string GetLibVersion(void);

   /**
    * @brief Standard getter method which return the current version
    *
    * @return std::string  Return nice formatted version
    *                      of the application (vx_xxrx)
    *
    * TODO: Make sure that the version is only returned in the above mentioned
    *       format (and not vxx_xxxxxrxxxx)
    */
   std::string GetVersion(void) const;

   /**
    * @brief Get the Raw Version object
    *
    * @return std::array<int, 3>
    * [0]=VERSION_MAJOR
    * [1]=VERSION_MINOR
    * [2]=VERSION_REVISION
    */
   std::array<int, 3> GetVersionRaw(void) const;

   /**
    * @brief Print the version of the application to given output stream
    * reference
    *
    * @param ros_OutputStream  Reference to output stream
    * @return std::ostream&    Return nice formatted version
    *                          of the application (vx_xxrx)
    */
   std::ostream& PrintVersion(std::ostream& ros_OutputStream) const;

  protected:
   /**
    * @brief Construct a new Version object
    *
    * @param ou8_Major        Major version of the application
    * @param ou8_Minor        Minor version of the application
    * @param ou8_Revision     Revision of the application
    */
   Version(int ou8_Major = 0, int ou8_Minor = 0, int ou8_Revision = 0)
       : au8_Version{ou8_Major, ou8_Minor, ou8_Revision}
   {}

   /**
    * @brief Destroy the Version object
    *
    */
   ~Version() = default;

  private:
   std::array<int, 3> au8_Version{};
};

}  // namespace Application

#endif  // __VERSION Headerguard
