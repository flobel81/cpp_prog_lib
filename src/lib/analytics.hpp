/*
 * Copyright (C) 2021-2022 The developers
 *
 * Author:
 *  Florian Belser
 *
 * Source:
 *  https://github.com/flobel81/cpp_prog_lib.git
 *
 * This file is a part of the cpp_prog_lib library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 */

#ifndef __ANALYTICS  // Headerguard
#define __ANALYTICS

#include <chrono>
#include <iostream>
#include <ratio>

namespace Analytics
{
/**
 * @brief Timer class for measuring execution time of analysed function
 *
 * @tparam TimePrecission     Supported ratio types ard std::milli, std::micro
 *                            and std::nano (default std::ratio<1,1>)
 * @tparam ClockType          All supported chrono clock types
 */
template <typename TimePrecission = std::ratio<1, 1>,
          typename ClockType = std::chrono::steady_clock>
class Timer
{
  public:
   /**
    * @brief Construct a new Timer object for analysing
    *        the function execution period
    *
    * @param opcn_File      default is __builtin_FILE()
    * @param opcn_Function  default is __builtin_FUNCTION()
    */
   Timer(char const* opcn_File = __builtin_FILE(),
         char const* opcn_Function = __builtin_FUNCTION())
       : c_FileName{opcn_File}, c_FunctionName{opcn_Function}
   {
      c_StartTimer = ClockType::now();
      if (std::ratio_equal<TimePrecission, std::ratio<1, 1>>::value)
      {
         c_Unit = "s";
      }
      else if (std::ratio_equal<TimePrecission, std::milli>::value)
      {
         c_Unit = "ms";
      }
      else if (std::ratio_equal<TimePrecission, std::micro>::value)
      {
         c_Unit = "us";
      }
      else if (std::ratio_equal<TimePrecission, std::nano>::value)
      {
         c_Unit = "ns";
      }
      else
      {
         c_Unit = "unknown";
      }
   }

   /**
    * @brief Destroy the Timer object
    *
    */
   ~Timer()
   {
      c_EndTimer = ClockType::now();
      std::chrono::duration<double, TimePrecission> c_Duration =
          c_EndTimer - c_StartTimer;
      std::cout << "Execution time of " << c_FileName << "/" << c_FunctionName
                << ": " << c_Duration.count() << " " << c_Unit << "\n";
   }

  private:
   std::chrono::time_point<ClockType> c_StartTimer;
   std::chrono::time_point<ClockType> c_EndTimer;
   std::string c_Unit;
   std::string c_FileName;
   std::string c_FunctionName;
};
}  // namespace Analytics

#endif  // __ANALYTICS Headerguard
