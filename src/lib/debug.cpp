/*
 * Copyright (C) 2021-2022 The developers
 *
 * Author:
 *  Florian Belser
 *
 * Source:
 *  https://github.com/flobel81/cpp_prog_lib.git
 *
 * This file is a part of the cpp_prog_lib library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 */

#include "debug.hpp"

namespace Application
{
// --------------- CLASS Debug --------------------------------
// --------------------------------------------------------------
Debug::Debug(Debug_Mode oc_DebugMode, std::string oc_FilePath)
    : c_DebugMode{oc_DebugMode}, c_Output{std::cout.rdbuf()}
{
   if (!oc_FilePath.empty())
   {
      c_OutputFile.open(oc_FilePath, std::ofstream::out | std::ofstream::app);
      // check if it was possible to open the file
      if (c_OutputFile.is_open() == true)
      {
         c_Output.rdbuf(c_OutputFile.rdbuf());
      }
   }
}

Debug::Debug() : c_DebugMode{Debug_Mode::OFF}, c_Output{std::cout.rdbuf()} {}

Debug::~Debug()
{
   if (c_OutputFile.is_open() == true)
   {
      // restore cout buffer
      c_Output.rdbuf(std::cout.rdbuf());
      // close previously openend file
      c_OutputFile.close();
   }
}

Debug_Mode Debug::GetDebugMode(void) const
{
   return c_DebugMode;
}

void Debug::SetDebugMode(const Debug_Mode& roc_DebugMode)
{
   c_DebugMode = roc_DebugMode;
}

void Debug::SetLogFile(const std::string& roc_LogFile)
{
   if (c_OutputFile.is_open() == true)
   {
      // restore cout buffer
      c_Output.rdbuf(std::cout.rdbuf());
      // close previously openend file
      c_OutputFile.close();
   }
   if (!roc_LogFile.empty())
   {
      c_OutputFile.open(roc_LogFile, std::ofstream::out | std::ofstream::app);
      // check if it was possible to open the file
      if (c_OutputFile.is_open() == true)
      {
         c_Output.rdbuf(c_OutputFile.rdbuf());
         PrintDebug("Set Debug output to file " + roc_LogFile);
      }
   }
   else
   {
      PrintDebug("Set Debug output to std::cout");
   }
}

void Debug::IncreaseDebugMode(void)
{
   switch (c_DebugMode)
   {
      case Debug_Mode::OFF:
         c_DebugMode = Debug_Mode::ON;
         break;
      case Debug_Mode::ON:
         c_DebugMode = Debug_Mode::EXTENDED;
         break;
      case Debug_Mode::EXTENDED:
         c_DebugMode = Debug_Mode::MAX;
         break;
      case Debug_Mode::MAX:
         // We already reached max debug mode, do nothing
         break;
      default:
         // Should never be reached
         c_DebugMode = Debug_Mode::OFF;
         break;
   }
}

void Debug::Print(std::string oc_String,
                  char const* opcn_File /* = __builtin_FILE() */,
                  char const* opcn_Function /* = __builtin_FUNCTION() */,
                  int osnLine /* = __builtin_LINE()*/)
{
   switch (c_DebugMode)
   {
      case Debug_Mode::EXTENDED:
      case Debug_Mode::MAX:
         PrintDebug(oc_String, opcn_File, opcn_Function, osnLine);
         break;
      case Debug_Mode::ON:
      case Debug_Mode::OFF:
         c_Output << oc_String << "\n";
         break;
      default:
         break;
   }
}

void Debug::PrintDebug(std::string oc_String,
                       char const* opcn_File /* = __builtin_FILE() */,
                       char const* opcn_Function /* = __builtin_FUNCTION() */,
                       int osnLine /* = __builtin_LINE()*/)
{
   switch (c_DebugMode)
   {
      case Debug_Mode::EXTENDED:
         c_Output << "DEBUG: " << opcn_Function << "[" << osnLine
                  << "]: " << oc_String << "\n";
         break;
      case Debug_Mode::MAX:
         c_Output << "DEBUG_EXT: (" << opcn_File << ") " << opcn_Function << "["
                  << osnLine << "]: " << oc_String << "\n";
         break;
      case Debug_Mode::ON:
         c_Output << oc_String << "\n";
         break;
      case Debug_Mode::OFF:
      default:
         break;
   }
}
}  // namespace Application