/*
 * Copyright (C) 2021-2022 The developers
 *
 * Author:
 *  Florian Belser
 *
 * Source:
 *  https://github.com/flobel81/cpp_prog_lib.git
 *
 * This file is a part of the cpp_prog_lib library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 */

#ifndef __APPLICATION  // Headerguard
#define __APPLICATION

#include <iomanip>
#include <iostream>
#include <set>
#include <string>
#include <tuple>

#include "command_line.hpp"
#include "debug_facility.hpp"
#include "version.hpp"

namespace Application
{
/**
 * @brief Info class
 *
 */
class Info : public CommandLine::Parser, public Application::Version
{
  public:
   /**
    * @brief Construct a new Info object
    *
    * @param oc_Application_Name    Name of the application
    * @param roc_Version            Reference to the version of the application
    * @param roc_Debug              Reference to Debug class
    */
   Info(std::string oc_Application_Name, std::tuple<int, int, int>& roc_Version,
        const AppDebug& roc_Debug);

   /**
    * @brief Destroy the Info object
    *
    */
   ~Info() = default;

   /**
    * @brief Set the License object
    *
    * @param oc_License     Reference to the license of the application
    */
   void SetLicense(std::string& roc_License);

   /**
    * @brief Get the License object
    *
    * @return std::string    Return License of the application
    */
   std::string GetLicense(void) const;

   /**
    * @brief Print the help of the application
    *
    */
   virtual void Help();

   /**
    * @brief Print the manpage of the application
    *
    */
   virtual void Manpage();

   /**
    * @brief Implement functor to print version
    *
    * @param ros_OutputStream Reference to the output stream
    * @param roc_Info         Reference to the Info class object
    * @return std::ostream&
    */
   friend std::ostream& operator<<(std::ostream& ros_OutputStream,
                                   const Info& roc_Info)
   {
      return roc_Info.PrintVersion(ros_OutputStream);
   }

  private:
   std::string c_Application_Name{};
   AppDebug c_Debug;
   std::string c_License{};

   void PrintVersionInt() const;
   void IncreaseDebugLevelInt() const;
};

}  // namespace Application

#endif  // __APPLICATION Headerguard
