## General

`Provide a detailed description`

### What would be the benefit if the feature is implemented

`Often it is really helpful to describe how the feature could help to reach the next level of the library`

### Additional information

`Code snippets, pictures, logs`
