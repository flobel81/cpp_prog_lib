# cpp_prog_lib library
## Introduction
The cpp_prog_lib library is a library that provides basic functionality for c++ based applications / daemons like
* help / manpage
* standardized command line parsing
* debug functionality
* version handling functionality
* timestamp functionality
* analytics functionality

The idea is to have the above mentioned functionality encapsulated so that every application which uses the cpp_prog_lib library has the same "look and feel" and more important, work has not to be done twice.

## License

Please refer to the library LICENSE file and the example application LICENSE file for further information.

## How to build

To build the library, additional packages has to be installed before cmake could be executed.
```shell
apt update && apt -y install cmake valgrind doxygen graphviz texlive-latex-base texlive-fonts-recommended texlive-fonts-extra texlive-latex-extra
```
### cpp_prog_lib library and documentation

```bash
# Toplevel
$ mkdir build
$ cd build
$ cmake ..
# Create libraries
$ make all
# Create documentation
$ make doc
```
### result
``` bash
# Create result
$ make install
# result directory
.
├── share
│   ├── doc
│   │   └── cpp_prog_base_lib.pdf
│   └── info
│       └── .clang-format
├── include
│   ├── analytics.hpp
│   ├── application.hpp
│   ├── command_line.hpp
│   ├── debug_facility.hpp
│   ├── debug.hpp
│   ├── timestamp.hpp
│   └── version.hpp
└── lib
    ├── shared
    │   ├── libcpp_prog_base_lib.so
    │   └── LICENSE
    └── static
        ├── libcpp_prog_base_lib_static.a
        └── LICENSE

```
### Example application

Switch to src/example directory and execute the following commands
``` shell
$ mkdir build
$ cd build
$ cmake ..
# Create example application
$ make all
# Create result
$ make install

# result directory
bin
├── config.toml
└── Demo
```
## Testing
Basic test like detecting memory leaks are performed with valgrind
https://valgrind.org/
```shell
$ valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes -s bin/Demo
```

## How to contribute as a user of the library

As a non project member you can also contribute to the project by providing bug reports or new feature requests. For that, you can use the service desk functionality of the project.
Please send your issue or feature request to the mail address below:

**E-Mail: contact-project+flobel81-cpp-prog-lib-33660443-issue-@incoming.gitlab.com**

The mail subject will be automatically mapped to the subject of the issue. Additional, please use the following E-Mail body style for your requests:

### Feature
```
## General

`Detailed description`

### What would be the benefit of the feature

`Often it is really helpful to describe how the feature could help to reach the next level of the library`

### Additional information

```

### Issue
```
## General

`Detailed description`

### How could the issue be reproduced

`Step by step instruction to reproduce the issue`

### Idea, concept to solve the issue

`If already possible, describe how the issue could be solved or provide diffs`

### Additional information

```


## How to contribute as a project member

* Use pre defined templates for creating a new issue
* Every issue shall be handled in a separated branch
* A branch name shall start with the issue number, followed by the branch topic
* The branch name shall be written in lower case
* A hyphen shall be used as separator within the branch name
  * <issue_number>-/<topic\>
* Code shall be formated with the tool "clang-format"
  * .clang-format is part of the project and shall be used
* Merge Request shall only be merged in case of CI is passed


## Used libraries

* https://github.com/HowardHinnant/date.git \
Date library is used to provide different kind of timestamps within the timestamp.hpp header like an ISO8601 conform one.
   * The date lib is released under the terms of the MIT license (https://github.com/HowardHinnant/date/blob/master/LICENSE.txt)
* https://github.com/ToruNiina/toml11.git \
toml11 library provides functionality to parse toml configuration files. The toml library is not used within the library itself. The library is only used to provide an toml based configuration file for the example application.
   * The toml11 lib is released under the terms of the MIT license (https://github.com/ToruNiina/toml11/blob/master/LICENSE)
